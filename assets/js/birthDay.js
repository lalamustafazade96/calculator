function calculateAge(){
	let bdate = document.getElementById("birthday").value;
	let result = datedif(bdate);
	document.getElementById("age").value = result;
}

function datedif(birthday){
	 var ageDif = Date.now() - Date.parse(birthday);
    var ageDate = new Date(ageDif);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}