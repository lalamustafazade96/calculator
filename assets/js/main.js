let variable1 = "";
let variable2 = "";
let op;
function result(a,b,op){
	switch (op){
		case "+": return (a + b); break;
		case "-": return (a - b); break;
		case "*": return (a * b); break;
		case "/": return (a / b); break;
	}
}

function getValue(id){
	let x = id.value;
	let netice;
	let y = document.getElementById("input1");
	if(isNaN(x)){
		if (x == "=") {
			variable2 = y.value;
			netice = result(parseInt(variable1),parseInt(variable2),op);
			y.value = netice;
		}
		else if (x == "clean"){
			y.value = "";
		}
		else {
			op = x;
			variable1 = y.value;
			y.value = ""; 
		}
	}
	else{
		y.value += x;
	}
}
